#ifndef WORDS_H
#define WORDS_H

void load_words_list(char *file_path);
void free_words_list();
char **load_words(size_t n);
void free_words(size_t n, char **words);
char *load_words_string(size_t n, char **words);
void free_words_string(char *words_string);
char *gen_words_string(size_t n);
char *gen_ordered_words_string(size_t n);

size_t get_word_list_sz();

#endif
