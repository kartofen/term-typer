#include "terminal.h"
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

static struct termios old_tio, new_tio;

void init_terminal()
{
    tcgetattr(STDIN_FILENO,&old_tio);
    new_tio=old_tio;
    new_tio.c_lflag &= ( ~ICANON & ~ECHO );
    tcsetattr(STDIN_FILENO,TCSANOW,&new_tio);
}

void destroy_terminal()
{
    tcsetattr(STDIN_FILENO,TCSANOW,&old_tio);
    printf("\e[0m");
    shwcur();
    fflush(stdout);
}

#ifdef __WIN32
#include <windows.h>
    int getrows()
    {
          CONSOLE_SCREEN_BUFFER_INFO csbi;
          GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
          return csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
    }

    int getclmns()
    {
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
        return csbi.srWindow.Right - csbi.srWindow.Left + 1;
    }
#else
    int getrows()
    {
        struct winsize w;
        ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
        return w.ws_row;
    }

    int getclmns()
    {
        struct winsize w;
        ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
        return w.ws_col;
    }

    char getche()
    {
        return fgetc(stdin);
    }
#endif
