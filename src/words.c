#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include "words.h"

size_t WORDS_LIST_SZ = 0;
char **words_list;

void load_words_list(char *file_path)
{
    FILE *fp = fopen(file_path, "r");
    if(!fp) {
        fprintf(stderr, "ERROR: Could not open file: %s\n", file_path);
        exit(EXIT_FAILURE);
    }

    char line[512];
    fgets(line, sizeof(line), fp);
    WORDS_LIST_SZ = atoi(line);
    words_list = malloc(WORDS_LIST_SZ * sizeof(char*));

    char delim[16];
    fgets(line, sizeof(line), fp);
    strcpy(delim, line);

    delim[strlen(delim)-1] = '\0';
    delim[strlen(delim)-1] = '\0';

    size_t i = 0;
    while(fgets(line, sizeof(line), fp) != NULL)
    {
        char *tok = strtok(line, delim);
        while(tok != NULL)
        {
            assert(i < WORDS_LIST_SZ);
            if(tok[strlen(tok)-1] == '\n') tok[strlen(tok)-1] = '\0';

            words_list[i] = malloc(strlen(tok)+1);
            memset(words_list[i], 0, strlen(tok)+1);

            strcpy(words_list[i], tok);
            i++;
            tok = strtok(NULL, delim);
        }
    }

    fclose(fp);

    srand(time(0));
}

void free_words_list()
{
    for(size_t i = 0; i < WORDS_LIST_SZ; i++)
        free(words_list[i]);

    free(words_list);
}

char **load_words(size_t n)
{
    assert(WORDS_LIST_SZ != 0);
    assert(n != 0);

    char **words = malloc(n * sizeof(char *));

    for(size_t i = 0; i < n; i++)
    {
        char *word = words_list[rand() % WORDS_LIST_SZ];
        words[i] = malloc(strlen(word)+1);
        memset(words[i], 0, strlen(word)+1);
        strcpy(words[i], word);
    }

    return words;
}

void free_words(size_t n, char **words)
{
    for(size_t i = 0; i < n; i++)
        free(words[i]);

    free(words);
}

char *load_words_string(size_t n, char **words)
{
    size_t words_s_sz = 0;

    for(size_t i = 0; i < n; i++)
        words_s_sz += strlen(words[i]) + 1;

    char *words_s = malloc(words_s_sz);

    int ws_i = 0;
    for(size_t i = 0; i < n; i++, ws_i++)
    {
        for(size_t j = 0; j < strlen(words[i]); j++, ws_i++)
            words_s[ws_i] = words[i][j];

        words_s[ws_i] = ' ';
    }
    words_s[ws_i - 1] = '\0';

    return words_s;
}

void free_words_string(char *words_string)
{
    free(words_string);
}

char *gen_words_string(size_t n)
{
    char **words = load_words(n);
    char *words_s = load_words_string(n, words);

    free_words(n, words);

    return words_s;
}

char *gen_ordered_words_string(size_t n)
{
    assert(n <= WORDS_LIST_SZ);
    char *words = load_words_string(n, words_list);
    return words;
}

size_t get_word_list_sz()
{
    return WORDS_LIST_SZ;
}
