#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "terminal.h"
#include "words.h"

size_t WORDS_SZ = 0;

int main(int argc, char **argv)
{
    if(argc > 2) {
        load_words_list(argv[2]);
    } else {
        fprintf(stderr, "ERROR: Please provide enough options and arguments\n\n");
        puts("Usage: ttypr [OPTION] [FILE] [N](OPTIONAL)");
        puts("\nOptions:");
        puts(" -r  randmoize the words from FILE and test used on N words");
        puts(" -o  test the user on N words from FILE ");
        puts("\nIf N is not set, it is equal the the amount of words in FILE");
        exit(EXIT_FAILURE);
    }

    if(argc == 4) {
        WORDS_SZ = atoi(argv[3]);
    } else {
        WORDS_SZ = get_word_list_sz();
    }

    init_terminal();

    while(1)
    {
        char *words;

        if(strcmp(argv[1], "-r") == 0)
            words = gen_words_string(WORDS_SZ);
        else if(strcmp(argv[1], "-o") == 0)
            words = gen_ordered_words_string(WORDS_SZ);

        clrscr();     gohome();
        puts(words);  gohome();
        hdcur();

        time_t t1 = 0;
        size_t i = 0;
        while(i < strlen(words))
        {
            char ch = getche(stdin);
            if(t1 == 0) t1 = time(0);

            if(ch >= 32 && ch <= 126)
            {
                if(ch == words[i])
                    putc_correct(words[i]);
                else
                    putc_wrong(words[i]);

                i++;
            }
            else if(ch == 127 && i != 0)
            {
                i--;

                gotoxy(i % getclmns(), i / getclmns());
                putchar(words[i]);
                gotoxy(i % getclmns(), i / getclmns());
            }
            else if(ch == 27)
            {
                clrscr(); gohome();
                goto destroy;
            }
        }

        time_t t = time(0)-t1;
        float wpm = WORDS_SZ/(t/60.0f);
        clrscr(); gohome();

        printf("You wrote %ld words for %ld seconds\n", WORDS_SZ, t);
        printf("Your WPM are %f\n", wpm);

    destroy:
        free_words_string(words);

        break;
    }

    free_words_list();
    destroy_terminal();
    return 0;
}
