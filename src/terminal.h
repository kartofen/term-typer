#ifndef TERMINAL_H
#define TERMINAL_H

#include <stdio.h>

void init_terminal();
void destroy_terminal();
int getrows();
int getclmns();

#ifdef __WIN32
#include <conio.h>
#else
char getche();

static inline void clrscr()
{
    printf("\e[2J");
}

static inline void gotoxy(int x, int y)
{
    printf("\e[%d;%dH", y+1, x+1);
}
#endif

static inline void gohome()
{
    printf("\e[H");
}

static inline void hdcur()
{
    printf("\e[?25l");
}
static inline void shwcur()
{
    printf("\e[?25h");
}

static inline void putc_wrong(char ch)
{
    printf("\e[31;4m%c\x1b[0m", ch);
}

static inline void putc_correct(char ch)
{
    printf("\e[32;4m%c\x1b[0m", ch);
}

#endif
