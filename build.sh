#!/bin/sh
cd ${0%/*} # go to project root
set -xe

FLAGS="-Wall -Wextra -g"
SRCD="src"
ODIR="obj"
BIN="bin"

function clean {
    rm -rf $BIN
    rm -rf $ODIR
}

if ! { [[ $# -eq 0 ]]; } 2> /dev/null
then
    ($1)
    exit 0;
fi


mkdir -p $ODIR
mkdir -p $BIN

gcc -c $SRCD/terminal.c -o $ODIR/terminal.o $FLAGS
gcc -c $SRCD/words.c    -o $ODIR/words.o    $FLAGS
gcc -c $SRCD/main.c     -o $ODIR/main.o     $FLAGS
gcc -o $BIN/ttypr $ODIR/main.o $ODIR/terminal.o $ODIR/words.o $FLAGS
